<?php
/* Template Name: Location Page */ 
global $wpc_option;

$page_layout     = get_post_meta( $post->ID, '_wpc_page_layout', true );
$page_breadcrumb = get_post_meta( $post->ID, '_wpc_hide_breadcrumb', true );
$page_comment    = wpcharming_option('page_comments');

wpcharming_get_header() ?>

		<?php
		global $post;
		if ($post->post_parent == 17 || $post->post_title == "Contact Us") {
			wpcharming_get_page_header_with_title($post->ID);
		} else {
			wpcharming_get_page_header_for_location($post->ID);
		}
		?>

		<?php if ( $page_breadcrumb !== 'on' ) wpcharming_breadcrumb(); ?>

		<div id="content-wrap" class="<?php echo ( $page_layout == 'full-screen' ) ? '' : 'container'; ?> <?php echo wpcharming_get_layout_class(); ?>">
			<div id="primary" class="<?php echo ( $page_layout == 'full-screen' ) ? 'content-area-full' : 'content-area'; ?><?=($post->post_title == 'Locations') ? ' nopadtop' : ''?> location-page" data-location="<?=get_the_title($post->ID)?>">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>
						<?php
						if ($post->post_parent == 17) {
						?>
						<div class="custom-heading wpb_content_element custom-service">
							<h2 class="heading-title"><?=$post->post_title?></h2>
							<span class="heading-line primary"></span>
						</div>
						<?php
						}
						?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

							<div class="entry-content">

								<?php the_content(); ?>

							</div><!-- .entry-content -->

						</article><!-- #post-## -->
					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->
<?php
	$phone = get_phone_number(get_the_title($post->ID));
	// if (!isset($phone) || empty($phone)) {
	// 	$phone = "01243 282 818";
	// }
	if (!isset($email) || empty($email)) {
		$email = "info@drainageplus.co.uk";
	}
?>
<!-- 			<div id="secondary" class="widget-area sidebar" role="complementary">
				<div class="services-sidebar">
					<h4>Contact Drainage Plus <?=$post->post_title?></h4>
					<div class="contact-info-box">
						<p>
							We're available to help you 24 hours a day,	seven days a week.
						</p>
						<div class="contact-info-item">
							<div class="contact-text"><i class="fa fa-phone"></i></div>
							<div class="contact-value"><a href="tel:<?=$phone?>" class="phone"><?=$phone?></a></div>
						</div>
						<div class="contact-info-item">
							<div class="contact-text"><i class="fa fa-envelope"></i></div>
							<div class="contact-value"><a href="mailto:<?=$email?>"><?=$email?></a></div>
						</div>
					</div>
				</div>
			</div>

 -->

		</div> <!-- /#content-wrap -->

		<?php
			include('partials/location-bottom.php');
		?>

<?php get_footer(); ?>
