<div class="dp-imgtxtbtn" style="background-image:url(<?=reset($img)?>)">
	<div class="container clearfix">
		<div class="txtholder">
			<h4><?=$heading?></h4>
			<h2><?=$content?></h2>
<?php
	if (!empty($button)) {
?>
			<div class="dp-header-buttons">
				<a class="button btn-white-outline" href="<?=esc_url( home_url( '/' ) )?><?=$button['url']?>"><?=$button['name']?></a>
			</div>
<?php
	}
?>
		</div>
	</div>
</div>