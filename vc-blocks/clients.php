<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcClients extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_clients_mapping' ) );
        add_shortcode( 'vc_clients', array( $this, 'vc_clients_html' ) );
    }
     
    public function vc_clients_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Our Clients', 'text-domain'),
                'base' => 'vc_clients',
                'description' => __('List of clients', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'clients',
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'heading' => 'Client Name',
                                'param_name' => 'name',
                                'value' => ''
                            ),  
                            array(
                                'type' => 'textfield',
                                'heading' => 'URL',
                                'param_name' => 'url',
                                'value' => ''
                            ), 
                            array(
                                'type' => 'attach_image',
                                'heading' => 'Logo',
                                'param_name' => 'logo',
                                'value' => ''
                            )         
                        )
                    )
                )
            )
        );                                     
    } 

    public function vc_clients_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $clients = json_decode(rawurldecode($atts['clients']));
        include 'clients-view.php';
    } 
     
} 

new vcClients();    