<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcReviews extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_reviews_mapping' ) );
        add_shortcode( 'vc_reviews', array( $this, 'vc_reviews_html' ) );
    }
     
    public function vc_reviews_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Reviews', 'text-domain'),
                'base' => 'vc_reviews',
                'description' => __('Row with Google, Yell, and Trustpilot ratings', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                )
            )
        );                                     
    } 

    public function vc_reviews_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        include 'reviews-view.php';
    } 
     
} 

new vcReviews();    