<div class="dp-our-services">
	<div class="container clearfix row_inner">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="custom-heading wpb_content_element">
				<h2 class="heading-title">Our Services</h2>
				<span class="heading-line primary"></span>
			</div>
			<ul class="our-services">
<?php
	foreach($services as $s) {
		$class = str_replace(array('&','/',' '), '', strtolower($s->service));
		$url = esc_url( home_url( '/' ) ).'services/'.str_replace('&', 'and', str_replace(array(' ','/'), '-', strtolower($s->service)));
?>
				<li class="<?=$class?>">
					<a href="<?=$url?>">
						<div class="icon-block">
						</div>
						<p>
							<strong><?=$s->service?></strong><br />
							<span><?=$s->content?></span>
						</p>
					</a>
				</li>
<?php
	}
?>
				<li class="contact">
					<a href="<?=esc_url( home_url( '/' ) )?>contact-us">
						<div class="icon-block">
						</div>
						<p>
							<strong>How can we help?</strong><br />
							<span>Contact us for more information on our services.</span>
						</p>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>