<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcServices extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_services_mapping' ) );
        add_shortcode( 'vc_services', array( $this, 'vc_services_html' ) );
    }
     
    public function vc_services_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Our Services', 'text-domain'),
                'base' => 'vc_services',
                'description' => __('List of services with icons & descriptions', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'services',
                        'params' => array(
                            array(
                                'type' => 'service_dropdown',
                                'heading' => 'Service',
                                'param_name' => 'service',
                                'value' => ''
                            ),  
                            array(
                                'type' => 'textarea',
                                'heading' => 'Content',
                                'param_name' => 'content',
                                'value' => ''
                            )            
                        )
                    )
                )
            )
        );                                     
    } 

    public function vc_services_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $services = json_decode(rawurldecode($atts['services']));
        include 'services-view.php';
    } 
     
} 

new vcServices();    