<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcLargecta extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_largecta_mapping' ) );
        add_shortcode( 'vc_largecta', array( $this, 'vc_largecta_html' ) );
    }
     
    public function vc_largecta_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Large Call to Action', 'text-domain'),
                'base' => 'vc_largecta',
                'description' => __('Large link with title and description', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'heading' => 'Heading',
                        'holder' => 'p',
                        'param_name' => 'heading',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textarea',
                        'heading' => 'Description',
                        'holder' => 'p',
                        'param_name' => 'description',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'URL',
                        'param_name' => 'url',
                        'value' => ''
                    )
                )
            )
        );                                     
    } 

    public function vc_largecta_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $heading = $atts['heading'];
        $description = $atts['description'];
        $url = $atts['url'];

        include 'largecta-view.php';
    } 
     
} 

new vcLargecta();    