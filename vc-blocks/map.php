<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcMap extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_map_mapping' ) );
        add_shortcode( 'vc_map', array( $this, 'vc_map_html' ) );
    }
     
    public function vc_map_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Map', 'text-domain'),
                'base' => 'vc_map',
                'description' => __('Map with pointers for each location', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'locations',
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'heading' => 'Name',
                                'param_name' => 'name',
                                'value' => ''
                            ),  
                            array(
                                'type' => 'textfield',
                                'heading' => 'Latitude',
                                'param_name' => 'lat',
                                'value' => ''
                            ), 
                            array(
                                'type' => 'textfield',
                                'heading' => 'Longitude',
                                'param_name' => 'lng',
                                'value' => ''
                            )         
                        )
                    )
                )
            )
        );                                     
    } 

    public function vc_map_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $map = json_decode(rawurldecode($atts['locations']));

        $points = [];

        foreach($map as $m) {
            $points[] = array(
                'title' => $m->name,
                'lat' => $m->lat,
                'lng' => $m->lng
            );
        }

        $points = json_encode($points);
        include 'map-view.php';
    } 
     
} 

new vcMap();    