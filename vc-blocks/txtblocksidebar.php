<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcTxtblocksidebar extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_txtblocksidebar_mapping' ) );
        add_shortcode( 'vc_txtblocksidebar', array( $this, 'vc_txtblocksidebar_html' ) );
    }
     
    public function vc_txtblocksidebar_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Location Page Content / Sidebar', 'text-domain'),
                'base' => 'vc_txtblocksidebar',
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'heading' => 'Phone Number',
                        'param_name' => 'phone',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Email',
                        'param_name' => 'email',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textarea_html',
                        'heading' => 'Content',
                        'holder' => 'p',
                        'param_name' => 'content',
                        'value' => ''
                    )
                )
            )
        );                                     
    } 

    public function vc_txtblocksidebar_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $phone = $atts['phone'];
        $email = $atts['email'];

        include 'txtblocksidebar-view.php';
    } 
     
} 

new vcTxtblocksidebar();    