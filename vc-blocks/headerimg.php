<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcHeaderimg extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_headerimg_mapping' ) );
        add_shortcode( 'vc_headerimg', array( $this, 'vc_headerimg_html' ) );
    }
     
    public function vc_headerimg_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Header Image', 'text-domain'),
                'base' => 'vc_headerimg',
                'description' => __('Header Image', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'attach_image',
                        'heading' => 'Background Image',
                        'param_name' => 'img',
                        'holder' => 'img'
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Heading',
                        'holder' => 'p',
                        'param_name' => 'heading',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textarea_html',
                        'heading' => 'Content',
                        'holder' => 'p',
                        'param_name' => 'content',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Button 1 Title',
                        'param_name' => 'button_1_title',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Button 1 URL',
                        'param_name' => 'button_1_url',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Button 2 Title',
                        'param_name' => 'button_2_title',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Button 2 URL',
                        'param_name' => 'button_2_url',
                        'value' => ''
                    ),
                )
            )
        );                                     
    } 

    public function vc_headerimg_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $img = wp_get_attachment_image_src($atts['img'], 'full');
        $heading = $atts['heading'];
        $title = $atts['title'];
        $button_1 = [];
        $button_2 = [];

        if (!empty($atts['button_1_title']) && !empty($atts['button_1_url'])) {
            $button_1 = array(
                'name' => $atts['button_1_title'],
                'url' => $atts['button_1_url']
            );
        }

        if (!empty($atts['button_2_title']) && !empty($atts['button_2_url'])) {
            $button_2 = array(
                'name' => $atts['button_2_title'],
                'url' => $atts['button_2_url']
            );
        }
        include 'headerimg-view.php';
    } 
     
} 

new vcHeaderimg();    