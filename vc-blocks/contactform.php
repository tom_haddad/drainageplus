<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcContactform extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_contactform_mapping' ) );
        add_shortcode( 'vc_contactform', array( $this, 'vc_contactform_html' ) );
    }
     
    public function vc_contactform_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Contact Form', 'text-domain'),
                'base' => 'vc_contactform',
                'description' => __('', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                )
            )
        );                                     
    } 

    public function vc_contactform_html( $atts, $content, $tag ) {
        include 'contactform-view.php';
    } 
     
} 

new vcContactform();    