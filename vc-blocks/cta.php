<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcCta extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_cta_mapping' ) );
        add_shortcode( 'vc_cta', array( $this, 'vc_cta_html' ) );
    }
     
    public function vc_cta_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Call to Action', 'text-domain'),
                'base' => 'vc_cta',
                'description' => __('Short bar with call to action text / button', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'textfield',
                        'heading' => 'Heading',
                        'holder' => 'p',
                        'param_name' => 'heading',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Button Title',
                        'param_name' => 'button_title',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Button URL',
                        'param_name' => 'button_url',
                        'value' => ''
                    )
                )
            )
        );                                     
    } 

    public function vc_cta_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $heading = $atts['heading'];
        $button = [];

        if (!empty($atts['button_title']) && !empty($atts['button_url'])) {
            $button = array(
                'name' => $atts['button_title'],
                'url' => $atts['button_url']
            );
        }

        include 'cta-view.php';
    } 
     
} 

new vcCta();    