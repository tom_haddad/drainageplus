<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcTestimonials extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_testimonials_mapping' ) );
        add_shortcode( 'vc_testimonials', array( $this, 'vc_testimonials_html' ) );
    }
     
    public function vc_testimonials_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Testimonials', 'text-domain'),
                'base' => 'vc_testimonials',
                'description' => (''), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
						"type"        => "exploded_textarea",
						"holder"      => "",
						"heading"     => __("Name for each Testimonial", 'js_composer'),
						"param_name"  => "names",
						"value"       => __("Mark Geragos,Evan Chesler,James M. Beck", 'js_composer'),
						"description" => __("Enter name for each testimonial here. Divide each with linebreaks (Enter).", 'js_composer')
					),
					array(
						"type"        => "attach_images",
						"heading"     => __("Testimonial Avatar", "js_composer"),
						"param_name"  => "images",
						"value"       => "",
						"description" => __("Select images from media library.", "js_composer")
					),
					array(
						"type"        => "attach_images",
						"heading"     => __("Testimonial Logo", "js_composer"),
						"param_name"  => "images2",
						"value"       => "",
						"description" => __("Select images from media library.", "js_composer")
					),
					array(
						"type"        => "textarea_html",
						"holder"      => "div",
						"heading"     => __("Testimonial Content", "js_composer"),
						"param_name"  => "content",
						"value"       => __("Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with ‘real’ content. This is required when, for example, the final text is not yet available. \n\n Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with ‘real’ content. This is required when, for example, the final text is not yet available. \n\n Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with ‘real’ content. This is required when, for example, the final text is not yet available.", "js_composer"),
						"description" => __("Enter content for each testimonial here. Divide each with linebreaks (Enter).", "js_composer")
					),

					array(
						"type"        => "checkbox",
						"heading"     => __("Carousel Autoplay","js_composer"),
						"value"       => array( __("Yes.","js_composer") => "yes" ),
						"param_name"  => "carousel_autoplay",
						'description'    => __( 'Autoplay testimonial carousel, Note: Carousel layout only display when you have more than one testimonial', 'js_composer' ),
					),

					array(
						"type"			 => "textfield",
						"class"			 => "",
						"heading"		 => __("Carousel Autoplay Speed","js_composer"),
						"param_name"	 => "carousel_autoplay_speed",
						"value"			 => "3000",
						'description'    => __( 'Carousel Autoplay Speed in millisecond', 'js_composer' ),
					),
					array(
						"type"			=> "textfield",
						"class"			=> "",
						"heading"		=> __("Carousel Speed","js_composer"),
						"param_name"	=> "carousel_speed",
						'description'    => __( 'Carousel Speed in millisecond', 'js_composer' ),
						"value"			=> "300",
					)
                )
            )
        );                                     
    } 

    public function vc_testimonials_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        extract( $atts );

		$output = null;
		$style_class = null;
		if ( $style == 'inverted' ) $style_class = ' inverted';
		$slick_rtl = 'false';
		if ( is_rtl() ){
			$slick_rtl = 'true';
		}

		$testimonial_avatars = explode(',', $images);
		$testimonial_logos = explode(',', $images2);
		$testimonial_names = explode( ',', $names );
		$testimonial_contents = preg_split("/\r\n|\n|\r/", wpb_js_remove_wpautop($content));
		$i = -1;

		if ( $carousel_autoplay == 'yes' ) {
			$carousel_autoplay = 'true';
		} else {
			$carousel_autoplay = 'false';
		}

		if ( $carousel_autoplay_speed == '' ) {
			$carousel_autoplay_speed = '3000';
		}

		if ( $carousel_speed == '' ) {
			$carousel_speed = '300';
		}

		$output .= '
		<div class="testimonial_carousel_wrapper">';

			$carousel_class = '';
			if ( count( $testimonial_contents > 1 ) ) {
				$carousel_class = 'testimonial_carousel_'.uniqid();
				$output .= '
				<script type="text/javascript">
					jQuery(document).ready(function(){
						"use strict";
						jQuery(".'. $carousel_class .'").slick({
							rtl: '. $slick_rtl .',
							autoplay: '. $carousel_autoplay .' ,
							autoplaySpeed: '. $carousel_autoplay_speed .' ,
							speed: '. $carousel_speed .' ,
							slidesToShow: 2,
							slidesToScroll: 1,
							draggable: false,
							prevArrow: "<span class=\'carousel-prev\'><i class=\'fa fa-angle-left\'></i></span>",
	        				nextArrow: "<span class=\'carousel-next\'><i class=\'fa fa-angle-right\'></i></span>",
	        				responsive: [
							    {
							      breakpoint: 640,
							      settings: {
							        slidesToShow: 1,
							        slidesToScroll: 1
							      }
							    }
							]
						});
					});
				</script>';
			}

			$output .= '
			<div class="'. $carousel_class .'">';

				foreach ( $testimonial_contents as $key => $value ) {
					$i++;
					if ( ! isset( $testimonial_contents[$i] ) ) {
						$testimonial_content = '';
					} else {
						$testimonial_content = $testimonial_contents[$i];
					}
					if ( ! isset( $testimonial_avatars[$i] ) ) $testimonial_avatars[$i] = '';
					if ( ! isset( $testimonial_logos[$i] ) ) $testimonial_logos[$i] = '';
					if ( ! isset( $testimonial_names[$i] ) ) $testimonial_names[$i] = '';


					$output .= '
					<div class="testimonial testimonial-item'. $style_class .'">';

						$output .= '
						<div class="testimonial-content">';

							$output .= '
							'. $testimonial_content .'';

						$output .= '
						</div>';

						$output .= '
						<div class="testimonial-header clearfix">';
						if ( $testimonial_logos[$i] != '' || $testimonial_avatars[$i] != '' ) {
								$output .= '<div class="testimonial-logo-face">';
								if ( $testimonial_logos[$i] != '' ) {
									$output .= '
									<div class="testimonial-logo"><img src="'. wp_get_attachment_url($testimonial_logos[$i]) .'" alt="'. esc_attr($testimonial_names[$i]) .'"></div>';
								}

								if ( $testimonial_avatars[$i] != '' ) {
									$output .= '
									<div class="testimonial-avatar"><img src="'. wp_get_attachment_url($testimonial_avatars[$i]) .'" alt="'. esc_attr($testimonial_names[$i]) .'"></div>';
								}
								$output .= '</div>';
							}
							$output .= '
							<div class="testimonial-name font-heading">'. $testimonial_names[$i] .'</div>';

						$output .= '
						</div>';

					$output .= '
					</div>';


				}

			$output .= '
			</div>';

		$output .= '
		</div>';


		return $output;
    } 
     
} 

new vcTestimonials();    