<div class="dp-our-clients">
	<div class="container clearfix row_inner">
		<div class="custom-heading wpb_content_element">
			<h2 class="heading-title">Our Clients</h2>
			<span class="heading-line primary"></span>
		</div>
		<div class="client-list">
<?php
	foreach($clients as $c) {
?>
			<a href="<?=$c->url?>" title="<?=$c->name?>" style="background-image:url(<?=wp_get_attachment_url($c->logo)?>)">
			</a>
<?php
	}
?>
		</div>
	</div>
</div>