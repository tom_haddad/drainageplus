<div class="wpb_column vc_column_container vc_col-sm-12">
	<form class="contact-form" action="<?=esc_url( home_url( '/' ) )?>/wp-content/themes/construction-child/forms.php">
		<div class="formcontent">
			<div class="form-left">
				<input type="text" name="name" placeholder="Name *" required />
				<input type="email" name="email" placeholder="Email Address*" required />
				<input type="text" name="phone" placeholder="Phone *" required />
			</div>
			<div class="form-right">
				<input type="text" name="subject" placeholder="Subject *" required />
				<textarea name="message" placeholder="Message *" required></textarea>
				<button class="btn-highlight btn-no-shadow">Get a Job Done</button>
			</div>
		</div>
		<p class="themessage"></p>
	</form>
</div>