<?php
	global $post;
	$phone = get_phone_number(get_the_title($post->ID));
	// if (!isset($phone) || empty($phone)) {
	// 	$phone = "01243 282 818";
	// }
	if (!isset($email) || empty($email)) {
		$email = "info@drainageplus.co.uk";
	}
?>
<div id="content-wrap" class="container left-sidebar">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="custom-heading wpb_content_element custom-service">
				<h2 class="heading-title">About Drainage Plus <?=$post->post_title?></h2>
				<span class="heading-line primary"></span>
			</div>
			<article class="page type-page status-publish has-post-thumbnail hentry">
				<div class="entry-content">
					<div class="vc_row wpb_row vc_row-fluid  ">
						<div class="row_inner_wrapper  clearfix">
							<div class="row_inner row_center_content clearfix">
								<div class="wpb_column vc_column_container vc_col-sm-12">
									<div class="vc_column-inner ">
										<div class="wpb_wrapper">
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<?=$content?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .entry-content -->
			</article>
			<!-- #post-## -->
		</main>
		<!-- #main -->
	</div>
	<!-- #primary -->
	<div id="secondary" class="widget-area sidebar" role="complementary">
		<div class="services-sidebar">
			<h4>Contact Drainage Plus <?=$post->post_title?></h4>
			<div class="contact-info-box">
				<p>
					We're available to help you 24 hours a day,	seven days a week.
				</p>
				<div class="contact-info-item">
					<div class="contact-text"><i class="fa fa-phone"></i></div>
					<div class="contact-value"><a href="tel:<?=$phone?>" class="phone"><?=$phone?></a></div>
				</div>
				<div class="contact-info-item">
					<div class="contact-text"><i class="fa fa-envelope"></i></div>
					<div class="contact-value"><a href="mailto:<?=$email?>"><?=$email?></a></div>
				</div>
			</div>
		</div>
	</div>
	<!-- #secondary -->
</div>