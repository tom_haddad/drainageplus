<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcLocations extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_locations_mapping' ) );
        add_shortcode( 'vc_locations', array( $this, 'vc_locations_html' ) );
    }
     
    public function vc_locations_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Our Locations', 'text-domain'),
                'base' => 'vc_locations',
                'description' => __('List of locations', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'textarea',
                        'heading' => 'Introduction',
                        'param_name' => 'intro'
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'locations',
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'heading' => 'Name',
                                'param_name' => 'name',
                                'value' => ''
                            ),  
                            array(
                                'type' => 'textfield',
                                'heading' => 'Phone Number',
                                'param_name' => 'phone',
                                'value' => ''
                            ), 
                            array(
                                'type' => 'textfield',
                                'heading' => 'Email',
                                'param_name' => 'email',
                                'value' => ''
                            ), 
                            array(
                                'type' => 'textfield',
                                'heading' => 'Description',
                                'param_name' => 'description',
                                'value' => ''
                            )            
                        )
                    )
                )
            )
        );                                     
    } 

    public function vc_locations_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $locations = json_decode(rawurldecode($atts['locations']));
        $intro = $atts['intro'];
        include 'locations-view.php';
    } 
     
} 

new vcLocations();    