<div class="dp-header-img" style="background-image:url(<?=reset($img)?>)">
	<div class="container clearfix">
		<div class="txtholder">
			<h1><?=$heading?></h1>
			<p><?=$content?></p>
<?php
	if (!empty($button_1) || !empty($button_2)) {
?>
			<div class="dp-header-buttons">
<?php
		if (!empty($button_1)) {
?>
				<a class="button btn-highlight" href="<?=esc_url( home_url( '/' ) )?><?=$button_1['url']?>"><?=$button_1['name']?></a>
<?php
		}
?>
<?php
		if (!empty($button_2)) {
?>
				<a class="button btn-white-outline" href="<?=esc_url( home_url( '/' ) )?><?=$button_2['url']?>"><?=$button_2['name']?></a>
<?php
		}
?>
			</div>
<?php
	}
?>
		</div>
	</div>
</div>