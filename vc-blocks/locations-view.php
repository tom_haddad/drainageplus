<div class="dp-locations">
	<div class="container clearfix row_inner">
		<div class="locations-top">
			<div class="custom-heading wpb_content_element">
				<h2 class="heading-title">Our Locations</h2>
				<span class="heading-line primary"></span>
			</div>
			<div class="location-intro">
				<p>
					<strong>
						<?=$intro?>
					</strong>
				</p>
			</div>
		</div>
		<div class="locations-grid">
<?php
	foreach($locations as $l) {
		$url = strtolower(str_replace(' ','-', $l->name));
		$l->phone = get_phone_number($url);
?>
			<div class="location-item">
				<h4>Drainage Plus<br /><?=$l->name?></h4>
				<div class="contact-info-box">
					<p>
						<?=$l->description?>
					</p>
					<div class="contact-info-item">
						<div class="contact-text"><i class="fa fa-phone"></i></div>
						<div class="contact-value phone"><a href="tel:<?=$l->phone?>"><?=$l->phone?></a></div>
					</div>
					<div class="contact-info-item">
						<div class="contact-text"><i class="fa fa-envelope"></i></div>
						<div class="contact-value"><a href="mailto:<?=$l->email?>"><?=$l->email?></a></div>
					</div>
				</div>
				<a href="<?=esc_url( home_url( '/' ) )?>locations/<?=$url?>" class="detail">View location detail</a>
			</div>
<?php
	}
?>
		</div>
	</div>
</div>