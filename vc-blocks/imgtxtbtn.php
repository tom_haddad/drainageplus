<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcImgtxtbtn extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_imgtxtbtn_mapping' ) );
        add_shortcode( 'vc_imgtxtbtn', array( $this, 'vc_imgtxtbtn_html' ) );
    }
     
    public function vc_imgtxtbtn_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Image w/ floating text', 'text-domain'),
                'base' => 'vc_imgtxtbtn',
                'description' => __('Large image with text and button', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'attach_image',
                        'heading' => 'Background Image',
                        'param_name' => 'img',
                        'holder' => 'img'
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Heading',
                        'holder' => 'p',
                        'param_name' => 'heading',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textarea_html',
                        'heading' => 'Content',
                        'holder' => 'p',
                        'param_name' => 'content',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Button Title',
                        'param_name' => 'button_title',
                        'value' => ''
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => 'Button URL',
                        'param_name' => 'button_url',
                        'value' => ''
                    )
                )
            )
        );                                     
    } 

    public function vc_imgtxtbtn_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $img = wp_get_attachment_image_src($atts['img'], 'full');
        $heading = $atts['heading'];
        $title = $atts['title'];
        $button = [];

        if (!empty($atts['button_title']) && !empty($atts['button_url'])) {
            $button = array(
                'name' => $atts['button_title'],
                'url' => $atts['button_url']
            );
        }

        include 'imgtxtbtn-view.php';
    } 
     
} 

new vcImgtxtbtn();    