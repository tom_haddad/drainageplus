<?php
	$main_phone = "01243 282 818";
	if (get_page_template_slug() == "location.php") {
		$main_phone = get_phone_number(get_the_title($post->ID));
	} else if (isset($_COOKIE['location'])) {
		$main_phone = get_phone_number($_COOKIE['location']);
	}
?>
<div class="dp-cta">
	<div class="container clearfix">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<h2><?=str_replace($main_phone, '<a href="tel:'.$main_phone.'">'.$main_phone.'</a>', $heading)?></h2>
<?php
		if (!empty($button)) {
?>
			<a class="button btn-dark btn-no-shadow" href="<?=esc_url( home_url( '/' ) )?><?=$button['url']?>"><?=$button['name']?></a>
<?php
		}
?>
		</div>
	</div>
</div>