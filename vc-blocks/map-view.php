<?php
	global $post;
	$points = [];

	if (get_the_ID() == 966) {
		$pages = get_pages(array(
		    'meta_key' => '_wp_page_template',
		    'meta_value' => 'location.php'
		));

		foreach($pages as $p) {
			$point = get_location_info(basename(get_permalink($p->ID)));

			$point['title'] = get_the_title($p->ID);

			$points[] = $point;
		}
	} else {
		$point = get_location_info($post->post_name);

		$point['title'] = get_the_title();
		$points[] = $point;
	}

	$points = json_encode($points);

?>
<div class="dp-location-map" data-points='<?=$points?>'>

</div>