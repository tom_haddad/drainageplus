<?php
/*
Element Description: Header image w/ text & buttons
*/
 
class vcFullwidthimg extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_fullwidthimg_mapping' ) );
        add_shortcode( 'vc_fullwidthimg', array( $this, 'vc_fullwidthimg_html' ) );
    }
     
    public function vc_fullwidthimg_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        vc_map( 
            array(
                'name' => __('Full Width Image', 'text-domain'),
                'base' => 'vc_fullwidthimg',
                'description' => __('', 'text-domain'), 
                'category' => __('Drainage Plus +', 'text-domain'),         
                'params' => array(   
                    array(
                        'type' => 'attach_image',
                        'heading' => 'Background Image',
                        'param_name' => 'img',
                        'holder' => 'img'
                    )
                )
            )
        );                                     
    } 

    public function vc_fullwidthimg_html( $atts, $content, $tag ) {
        $atts = vc_map_get_attributes($tag, $atts);
        $img = wp_get_attachment_image_src($atts['img'], 'full');

        include 'fullwidthimg-view.php';
    } 
     
} 

new vcFullwidthimg();    