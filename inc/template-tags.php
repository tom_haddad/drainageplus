<?php

if ( ! function_exists('wpcharming_get_page_title2') ) :
/**
 * Display the page title area at the top of single page with option.
 */
function wpcharming_get_page_title2($postID) {
	global $wpc_option;
	global $woocommerce;

	$display            = false;
	$button             = $class = null;
	$enable_page_header = get_post_meta( $postID, '_wpc_enable_page_header', true );
	$hide_page_title    = get_post_meta( $postID, '_wpc_hide_page_title', true );
	$admin_button_onoff = $wpc_option['page_title_contact'];
	$admin_button_pages = $wpc_option['page_contact_button'];
	$admin_button_style = $wpc_option['page_title_button_type'];
	$admin_button_text  = $wpc_option['page_title_button_text'];
	$admin_button_url   = $wpc_option['page_title_button_url'];

	if ( isset($admin_button_onoff) and $admin_button_onoff ) {
		if ( is_page( $admin_button_pages ) ) $display = true;
	}

	if ( $admin_button_style ) {
		$class = $admin_button_style;
	} else {
		$class = 'btn-light';
	}

	if ( $admin_button_text && $admin_button_url ) {
		$button = '<a href="'. get_permalink($admin_button_url) .'" class="btn '. $class .' right">'. esc_attr($admin_button_text) .'</a>';
	}

	if ( $hide_page_title != 'on' ) {
		?>
		<div class="page-title-wrap <?php if ( $display ) echo 'page-title-has-button'; ?>">
			<div class="container">
				<h1 class="page-entry-title left">
				<?php 
				if (!is_numeric($postID)) {
					echo $postID;
				} else {
					the_title();
				} ?>
				<?php
					$main_phone = "01243 282 818";
					if (get_page_template_slug() == "location.php") {
						$main_phone = get_phone_number(get_the_title($post->ID));
					} else if (isset($_COOKIE['location'])) {
						$main_phone = get_phone_number($_COOKIE['location']);
					}
				?>
				</h1>
				<a class="button btn-large btn-highlight btn-sharp-borders btn-no-shadow right btn-icon" href="tel:<?=$main_phone?>">
					<i class="fa fa-phone"></i> <?=$main_phone?>
				</a>
			</div>
		</div>
		<?php
	}
}
endif;

function wpcharming_get_page_header_for_location($postID) {
	$enable_page_header      = get_post_meta( $postID, '_wpc_enable_page_header', true );
	$header_title            = get_the_title($postID);
	$header_subtitle         = get_post_meta( $postID, '_wpc_header_title', true );
	$header_alignment        = get_post_meta( $postID, '_wpc_header_alignment', true );
	$header_bg               = get_post_meta( $postID, '_wpc_header_bg', true );
	$header_padding_top      = get_post_meta( $postID, '_wpc_header_padding_top', true );
	$header_padding_bottom   = get_post_meta( $postID, '_wpc_header_padding_bottom', true );
	$header_parallax         = get_post_meta( $postID, '_wpc_header_parallax', true );
	$header_parallax_overlay = get_post_meta( $postID, '_wpc_parallax_overlay', true );
	$header_bg_color         = get_post_meta( $postID, '_wpc_header_bg_color', true );
	$header_text_color       = get_post_meta( $postID, '_wpc_header_text_color', true );

	// Page Header CSS
	$page_header_style = array();
	if ( $header_bg_color ) $page_header_style[] = 'background-color: '. $header_bg_color .';';
	if ( $header_text_color ) $page_header_style[] = 'color: '. $header_text_color .';';
	if ( $header_padding_top ) $page_header_style[] = 'padding-top: '. $header_padding_top .'px;';
	if ( $header_padding_bottom ) $page_header_style[] = 'padding-bottom: '. $header_padding_bottom .'px;';
	if ( $header_alignment == 'center' ) $page_header_style[] = 'text-align: center;';
	if ( $header_alignment == 'right' ) $page_header_style[] = 'text-align: right;';
	if ( $header_bg && $header_parallax !== 'on' ) $page_header_style[] = 'background: url('. esc_url($header_bg) .') no-repeat center bottom;background-size:cover;';

	$page_header_style = implode('', $page_header_style);
	if ( $page_header_style ) {
		$page_header_style = wp_kses( $page_header_style, array() );
		$page_header_style = ' style="' . esc_attr($page_header_style) . '"';
	}

	// Parallax
	$parallax_bg = $data_bg = $wpc_row_parallax = $parallax_overlay = null;
	if ( $header_parallax == 'on' && $header_bg ) {
		$wpc_row_parallax = ' wpc_row_parallax';
		$data_bg     = ' data-bg="'. esc_url($header_bg) .'" data-speed="0.5"';
		$parallax_bg = '<div class="wpc_parallax_bg" style="background-image: url('. esc_url($header_bg) .')"></div>';

		if ( $header_parallax_overlay ) {
			$parallax_overlay = '<div class="wpc_video_color_overlay" style="background-color:'. $header_parallax_overlay .'"></div>';
		}

	}

	// Heading Color
	$heading_color = null;
	if ( $header_text_color ) {
		$heading_color = ' style="color:'. $header_text_color .'"';
	}

	//if ( $enable_page_header == 'on' ) {
		$icon_class = str_replace(array('&','/',' ','(',')'), '', strtolower($header_title));

		$phone = get_phone_number($header_title);
		echo '
		<div class="dp-header-img location-header" style="background-image: url('. esc_url($header_bg) .')">

			<div class="container clearfix">';
				echo '<div class="txtholder">';
				echo '<h1><strong>DRAINAGE PLUS</strong> '. $header_title .'</h1>';
				echo '<p>'. nl2br($header_subtitle) .'</p>';
					echo '<div class="dp-header-buttons">';
						echo '<a class="button btn-highlight btn-icon" href="tel:'.$phone.'"><i class="fa fa-phone"></i> '.$phone.'</a>';
						echo '<a class="button btn-highlight" href="'.esc_url( home_url( '/' ) ).'contact-us">Book a Job</a>';
					echo '</div>';
				echo '</div>';
			echo '
			</div>';
		echo '
		</div>';

	//}
}

function wpcharming_get_page_header_with_title($postID) {
	$enable_page_header      = get_post_meta( $postID, '_wpc_enable_page_header', true );
	$header_title            = (!empty(get_post_meta( $postID, '_wpc_header_title', true ))) ? get_post_meta( $postID, '_wpc_header_title', true ) : get_the_title($postID);
	$header_subtitle         = get_post_meta( $postID, '_wpc_header_subtitle', true );
	$header_alignment        = get_post_meta( $postID, '_wpc_header_alignment', true );
	$header_bg               = get_post_meta( $postID, '_wpc_header_bg', true );
	$header_padding_top      = get_post_meta( $postID, '_wpc_header_padding_top', true );
	$header_padding_bottom   = get_post_meta( $postID, '_wpc_header_padding_bottom', true );
	$header_parallax         = get_post_meta( $postID, '_wpc_header_parallax', true );
	$header_parallax_overlay = get_post_meta( $postID, '_wpc_parallax_overlay', true );
	$header_bg_color         = get_post_meta( $postID, '_wpc_header_bg_color', true );
	$header_text_color       = get_post_meta( $postID, '_wpc_header_text_color', true );

	// Page Header CSS
	$page_header_style = array();
	if ( $header_bg_color ) $page_header_style[] = 'background-color: '. $header_bg_color .';';
	if ( $header_text_color ) $page_header_style[] = 'color: '. $header_text_color .';';
	if ( $header_padding_top ) $page_header_style[] = 'padding-top: '. $header_padding_top .'px;';
	if ( $header_padding_bottom ) $page_header_style[] = 'padding-bottom: '. $header_padding_bottom .'px;';
	if ( $header_alignment == 'center' ) $page_header_style[] = 'text-align: center;';
	if ( $header_alignment == 'right' ) $page_header_style[] = 'text-align: right;';
	if ( $header_bg && $header_parallax !== 'on' ) $page_header_style[] = 'background: url('. esc_url($header_bg) .') no-repeat center bottom;background-size:cover;';

	$page_header_style = implode('', $page_header_style);
	if ( $page_header_style ) {
		$page_header_style = wp_kses( $page_header_style, array() );
		$page_header_style = ' style="' . esc_attr($page_header_style) . '"';
	}

	// Parallax
	$parallax_bg = $data_bg = $wpc_row_parallax = $parallax_overlay = null;
	if ( $header_parallax == 'on' && $header_bg ) {
		$wpc_row_parallax = ' wpc_row_parallax';
		$data_bg     = ' data-bg="'. esc_url($header_bg) .'" data-speed="0.5"';
		$parallax_bg = '<div class="wpc_parallax_bg" style="background-image: url('. esc_url($header_bg) .')"></div>';

		if ( $header_parallax_overlay ) {
			$parallax_overlay = '<div class="wpc_video_color_overlay" style="background-color:'. $header_parallax_overlay .'"></div>';
		}

	}

	// Heading Color
	$heading_color = null;
	if ( $header_text_color ) {
		$heading_color = ' style="color:'. $header_text_color .'"';
	}

	//if ( $enable_page_header == 'on' ) {
		$icon_class = str_replace(array('&','/',' ','(',')'), '', strtolower($header_title));
		echo '
		<div class="page-header-wrap services-header clearfix '. $wpc_row_parallax .'"'. $page_header_style . $data_bg .'>

			<div class="container">';
				if ($post->post_parent == 17) {
					echo '<div class="iconblock '.$icon_class.'"></div>';
				}
				echo '<h1 class="page-title">'. $header_title .'</h1>';
				echo '<span class="page-subtitle">'. wp_kses_post($header_subtitle) .'</span>';

			echo '
			</div>';

			echo $parallax_bg;

			echo $parallax_overlay;


		echo '
		</div>';

	//}
}