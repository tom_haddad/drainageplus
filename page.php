<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WPCharming
 */
global $wpc_option;

$page_layout     = get_post_meta( $post->ID, '_wpc_page_layout', true );
$page_breadcrumb = get_post_meta( $post->ID, '_wpc_hide_breadcrumb', true );
$page_comment    = wpcharming_option('page_comments');

wpcharming_get_header() ?>

		<?php
		global $post;
		if ($post->post_parent == 17 || $post->post_title == "Contact Us" || $post->post_title == "Commercial") {
			wpcharming_get_page_header_with_title($post->ID);
		} else {
			wpcharming_get_page_header($post->ID);
			wpcharming_get_page_title2($post->ID);
		}
		?>

		<?php if ( $page_breadcrumb !== 'on' ) wpcharming_breadcrumb(); ?>

		<div id="content-wrap" class="<?php echo ( $page_layout == 'full-screen' ) ? '' : 'container'; ?> <?php echo wpcharming_get_layout_class(); ?>">
			<div id="primary" class="<?php echo ( $page_layout == 'full-screen' ) ? 'content-area-full' : 'content-area'; ?><?=($post->post_title == 'Locations') ? ' nopadtop' : ''?>">
				<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>
						<?php
						if ($post->post_parent == 17) {
						?>
						<div class="custom-heading wpb_content_element custom-service">
							<h2 class="heading-title"><?=$post->post_title?></h2>
							<span class="heading-line primary"></span>
						</div>
						<?php
						}
						?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

							<div class="entry-content">

								<?php the_content();;?>

							</div><!-- .entry-content -->

						</article><!-- #post-## -->
					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->
			</div><!-- #primary -->

			<?php echo wpcharming_get_sidebar(); ?>



		</div> <!-- /#content-wrap -->

		<?php
			if ($post->post_parent == 17 || $post->post_title == "Contact Us") {
				include('partials/services-bottom.php');
			} else if ($post->post_parent == 968) {
				include('partials/guide-bottom.php');
			} else if ($post->post_title == "Commercial") {
				include('partials/commercial-bottom.php');
			}
		?>

<?php get_footer(); ?>
