<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'phpmailer/src/Exception.php';
require 'phpmailer/src/PHPMailer.php';
require 'phpmailer/src/SMTP.php';

$mail = new PHPMailer(true);

$entities = array();
$errors = array();
$message = "Thank you for your enquiry.<br />We will be in touch as soon as possible.";
$required = array("name","subject","email","phone","message");
$worked = 1;

foreach($_POST as $name => $value) {
	$value = htmlspecialchars($value);

    //if ($name !== "g-recaptcha-response") {
	   $entities[$name] = $value;
    //}

	if (in_array($name, $required) && empty($value)) {
		$errors[] = $name;
	} else {
		switch($name) {
			case 'email':
				$validator = new EmailAddressValidator;
				if (!$validator->check_email_address($value)) {
					$errors[] = "email";
				}
				break;
		}
	}
}

// if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
//     $secret = "6Leb1SAUAAAAAPgNFYCxf-0-ZOWZ9zVW1gCGNkyd";
//     $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
//     $responseData = json_decode($verifyResponse);
//     if ($responseData->success) {

//     } else {
//         $errors[] = 'recaptcha';
//         $worked = 0;
//     }
// } else {
//     $errors[] = 'recaptcha';
//     $worked = 0;
// }

if (count($errors) > 0) {
	$worked = 0;
} else {
	$maildata = array();
    $maildata['name'] = $entities['name'];
	$maildata['email'] = $entities['email'];
	$maildata['data'] = $entities;
	//sendMail($maildata);
    $worked = sendMail2($mail, $maildata);
}

$output = array(
	'worked' => $worked,
	'html' => $message,
	'errors' => $errors
);

echo json_encode($output);


function sendMail($maildata) {
	// $to = $maildata['email'];
    $to = $maildata['email'];
	$subject = "New Message [Drainage Plus Website]";

	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	$headers .= 'From: Drainage Plus+ <noreply@drainageplus.co.uk>' . "\r\n";
    $headers .= 'Reply-To: Drainage Plus+ <noreply@drainageplus.co.uk>' . "\r\n";
    $headers .= 'X-Mailer: PHP/' . phpversion();
	//$headers .= 'Bcc: hello@tomhaddad.com' . "\r\n";

	ob_start();
	include('email_template.php');
	$message = ob_get_contents();
	ob_end_clean();

	mail($to,$subject,$message,$headers);
}

function sendMail2($mail, $maildata) {
    try {
        //Server settings
        //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.tomhaddad.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'drainageplus.co.uk@mail.tomhaddad.com';                 // SMTP username
        $mail->Password = "C;!,4OLw'k5~DWD";                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('info@drainageplus.co.uk');
        $mail->addAddress($maildata['email'], $maildata['name']);     // Add a recipient
        $mail->addBCC('info@drainageplus.co.uk');

        ob_start();
        include('email_template.php');
        $message = ob_get_contents();
        ob_end_clean();

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'New Message [Drainage Plus Website]';
        $mail->Body    = $message;
        $mail->send();
        return 1;
    } catch (Exception $e) {
        return 0;
        // echo 'Message could not be sent.';
        // echo 'Mailer Error: ' . $mail->ErrorInfo;
    }
}


class EmailAddressValidator {

    /**
     * Check email address validity
     * @param   strEmailAddress     Email address to be checked
     * @return  True if email is valid, false if not
     */
    public function check_email_address($strEmailAddress) {
        
        // If magic quotes is "on", email addresses with quote marks will
        // fail validation because of added escape characters. Uncommenting
        // the next three lines will allow for this issue.
        //if (get_magic_quotes_gpc()) { 
        //    $strEmailAddress = stripslashes($strEmailAddress); 
        //}

        // Control characters are not allowed
        if (preg_match('/[\x00-\x1F\x7F-\xFF]/', $strEmailAddress)) {
            return false;
        }

        // Split it into sections using last instance of "@"
        $intAtSymbol = strrpos($strEmailAddress, '@');
        if ($intAtSymbol === false) {
            // No "@" symbol in email.
            return false;
        }
        $arrEmailAddress[0] = substr($strEmailAddress, 0, $intAtSymbol);
        $arrEmailAddress[1] = substr($strEmailAddress, $intAtSymbol + 1);

        // Count the "@" symbols. Only one is allowed, except where 
        // contained in quote marks in the local part. Quickest way to
        // check this is to remove anything in quotes.
        $arrTempAddress[0] = preg_replace('/"[^"]+"/'
                                         ,''
                                         ,$arrEmailAddress[0]);
        $arrTempAddress[1] = $arrEmailAddress[1];
        $strTempAddress = $arrTempAddress[0] . $arrTempAddress[1];
        // Then check - should be no "@" symbols.
        if (strrpos($strTempAddress, '@') !== false) {
            // "@" symbol found
            return false;
        }

        // Check local portion
        if (!$this->check_local_portion($arrEmailAddress[0])) {
            return false;
        }

        // Check domain portion
        if (!$this->check_domain_portion($arrEmailAddress[1])) {
            return false;
        }

        // If we're still here, all checks above passed. Email is valid.
        return true;

    }

    /**
     * Checks email section before "@" symbol for validity
     * @param   strLocalPortion     Text to be checked
     * @return  True if local portion is valid, false if not
     */
    protected function check_local_portion($strLocalPortion) {
        // Local portion can only be from 1 to 64 characters, inclusive.
        // Please note that servers are encouraged to accept longer local
        // parts than 64 characters.
        if (!$this->check_text_length($strLocalPortion, 1, 64)) {
            return false;
        }
        // Local portion must be:
        // 1) a dot-atom (strings separated by periods)
        // 2) a quoted string
        // 3) an obsolete format string (combination of the above)
        $arrLocalPortion = explode('.', $strLocalPortion);
        for ($i = 0, $max = sizeof($arrLocalPortion); $i < $max; $i++) {
             if (!preg_match('.^('
                            .    '([A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]' 
                            .    '[A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]{0,63})'
                            .'|'
                            .    '("[^\\\"]{0,62}")'
                            .')$.'
                            ,$arrLocalPortion[$i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks email section after "@" symbol for validity
     * @param   strDomainPortion     Text to be checked
     * @return  True if domain portion is valid, false if not
     */
    protected function check_domain_portion($strDomainPortion) {
        // Total domain can only be from 1 to 255 characters, inclusive
        if (!$this->check_text_length($strDomainPortion, 1, 255)) {
            return false;
        }
        // Check if domain is IP, possibly enclosed in square brackets.
        if (preg_match('/^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])'
           .'(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}$/'
           ,$strDomainPortion) || 
            preg_match('/^\[(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])'
           .'(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}\]$/'
           ,$strDomainPortion)) {
            return true;
        } else {
            $arrDomainPortion = explode('.', $strDomainPortion);
            if (sizeof($arrDomainPortion) < 2) {
                return false; // Not enough parts to domain
            }
            for ($i = 0, $max = sizeof($arrDomainPortion); $i < $max; $i++) {
                // Each portion must be between 1 and 63 characters, inclusive
                if (!$this->check_text_length($arrDomainPortion[$i], 1, 63)) {
                    return false;
                }
                if (!preg_match('/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|'
                   .'([A-Za-z0-9]+))$/', $arrDomainPortion[$i])) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check given text length is between defined bounds
     * @param   strText     Text to be checked
     * @param   intMinimum  Minimum acceptable length
     * @param   intMaximum  Maximum acceptable length
     * @return  True if string is within bounds (inclusive), false if not
     */
    protected function check_text_length($strText, $intMinimum, $intMaximum) {
        // Minimum and maximum are both inclusive
        $intTextLength = strlen($strText);
        if (($intTextLength < $intMinimum) || ($intTextLength > $intMaximum)) {
            return false;
        } else {
            return true;
        }
    }

}
