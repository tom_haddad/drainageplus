(function($) {
    'use strict';

    var mobileWidth = 646,
        tabletWidth = 876;

    $(document).on('ready', function() {
        DrainagePlus.init();
    });

    var DrainagePlus = {
        defaults: function() {
            $("a[href^='http://'],a[href^='https://']").each(function() {
                if ($(this).attr('href').indexOf('drainageplus') === 0) {
                    $(this).attr("target", "_blank");
                }
            });
            // $('.floatlabels input, .floatlabels textarea').floatlabel({
            //     labelClass: 'floatedlabel'
            // });
            $('.btt').on('click', function() {
                $("html, body").animate({
                    scrollTop: 0
                }, "slow");
            });
            // DrainagePlus.setClickables();
            // Couldn't see this code showing on live?
        },
        buttons: function() {
            var _ = this;
            $('.load-instagram').on('click', function() {
                _.instagram();
            });
            $("a[href*='enquiryform'], a.button:contains('Book a Job'), a.button:contains('Book a job')").on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                _.openModal();
            });
            $('body').on('click', function(e) {
                if ($('.modal-holder').is(':visible') && (!$(e.target).closest('.contact-modal').length || $(e.target).is('.close-modal'))) {
                    _.closeModal();
                }
            })
            $('.close-modal').on('click', function() {
                _.closeModal();
            });

        },
        checkFirstVisit: function() {
            if (document.cookie.indexOf("location") >= 0) {

            } else {
                if ($('.location-page').length) {
                    var loc = $('.location-page').data('location');
                    setCookie('location', loc);
                }
            }
        },
        closeModal: function() {
            var _ = this;
            $('.modal-holder').fadeOut(function() {
                $('body').removeClass('modal-open');
                $('.bodyholder').css('top', 0);
                $(window).scrollTop(_.oldScroll);
            });
        },
        forms: function() {
            $('form.contact-form').on('submit', function(e) {
                e.preventDefault();
                var theForm = $(this),
                    fd = new FormData(theForm[0]),
                    url = theForm.attr('action');

                theForm.find('input, textarea').each(function() {
                    $(this).removeClass('required');
                });

                theForm.find('.requiredtxt').hide();

                $.ajax({
                    type: "POST",
                    url: url,
                    data: fd,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        data = JSON.parse(data);
                        if (data.worked == 1) {
                            if (typeof window.ga === 'function') {
                                window.ga('send', 'event', 'Event', 'Button_click', 'Submit_form');
                            }
                            theForm.find('.themessage').html(data.html);
                            theForm.find('.formcontent').fadeOut(function() {
                                theForm.find('.themessage').fadeIn().css('display', 'block');
                            });
                        } else {
                            alert('Please check all fields are filled in correctly.');
                            for (var i = 0; i < data.errors.length; i++) {
                                theForm.find('[name=' + data.errors[i] + ']').addClass('required');
                            }
                        }
                    }
                });
            })
        },
        // contains code for ga events for email click and telephone click
        gaEvents: function() {
            var $emails = $('[href="mailto:info@drainageplus.co.uk"]');
            var $telephone = $('.phone, header .phone-text a');
            $emails.on('click', function() {
                if (typeof window.ga === 'function') {
                    window.ga('send', 'event', 'Event', 'Button_click', 'Email');
                }
            });
            $telephone.on('click', function() {
                if (typeof window.ga === 'function') {
                    window.ga('send', 'event', 'Event', 'Button_click', 'Call');
                }
            });
        },
        init: function() {
            var _ = this;

            _.checkFirstVisit();
            _.defaults();
            _.buttons();
            _.forms();
            _.theMap();
            _.resize();
            _.scroll();
            _.sliders();
            _.gaEvents();
            if ($('.dp-location-map').length) {
                _.map();
            }
            $(window).on('resize', function() {
                _.resize();
            });
            $(window).on('scroll', function() {
                _.scroll();
            });
        },
        instagram: function() {
            var hidden = $('.instagram-box.hidden'),
                i = 1,
                count = 2;

            if ($(window).width() > tabletWidth) {
                count = 4;
            }
            hidden.each(function() {
                if (i <= count) {
                    $(this).removeClass('hidden');
                }
                i++;
            });

            if (!$('.instagram-box.hidden').length) {
                $('.load-instagram').fadeOut();
            }

        },
        oldScroll: 0,
        openModal: function() {
            var _ = this;

            _.oldScroll = $(window).scrollTop();
            $('body').addClass('modal-open');
            $('.bodyholder').css('top', -_.oldScroll + 'px');
            $('.modal-holder').fadeIn();
        },
        map: function() {
            var _ = $('.dp-location-map'),
                points = _.data('points');
            Gmap.init(points);
        },
        resize: function() {
            var width = $(window).width();

            if (width > mobileWidth) {
                $('.dp-our-services li').css('height', getMaxHeight('.dp-our-services li'));
                $('.recent-news-wrapper p').css('height', getMaxHeight('.recent-news-wrapper p'));
                $('.locations-grid .location-item p').css('height', getMaxHeight('.locations-grid .location-item p'));
            } else {
                $('.dp-our-services li').css('height', '');
                $('.recent-news-wrapper p').css('height', '');
                $('.locations-grid .location-item p').css('height', '');
            }

            if (width > tabletWidth) {
                if ($('.dp-reviews .vc_column_container').hasClass('slick-initialized')) {
                    $('.dp-reviews .vc_column_container').slick('destroy');
                }
            } else {
                if (!$('.dp-reviews .vc_column_container').hasClass('slick-initialized')) {
                    $('.dp-reviews .vc_column_container').slick();
                }
            }
        },
        scroll: function() {

        },
        sliders: function() {

        },
        theMap: function() {
            $('.map-holder').click(function() {
                $('.map-holder iframe').css("pointer-events", "auto");
            });

            $(".map-holder").mouseleave(function() {
                $('.map-holder iframe').css("pointer-events", "none");
            });
        }
    };

    var Gmap = {
        init: function(points) {
            var map,
                markers = points,
                marker, i, theMarker = markers[0];

            if (markers.length > 1) {
                var bounds = new google.maps.LatLngBounds();
            }

            var mapOptions = {
                mapTypeId: 'roadmap',
                zoom: 9,
                center: new google.maps.LatLng(theMarker['lat'], theMarker['lng'])
            };

            // Display a map on the page
            map = new google.maps.Map($('.dp-location-map')[0], mapOptions);
            map.setTilt(45);

            var icon = {
                url: stylesheet_directory_uri + '/assets/img/map-pin.png',
                size: new google.maps.Size(50, 50),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(25, 40)
            };

            // Loop through our array of markers & place each one on the map  
            if (markers.length > 1) {
                for (i = 0; i < markers.length; i++) {
                    var position = new google.maps.LatLng(markers[i]['lat'], markers[i]['lng']);
                    bounds.extend(position);
                    marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        title: markers[i]['title'],
                        icon: icon
                    });
                }
            } else {
                var position = new google.maps.LatLng(theMarker['lat'], theMarker['lng']),
                    marker = new google.maps.Marker({
                        icon: icon,
                        position: position,
                        map: map,
                        title: theMarker['title']
                    });
            }


            if (markers.length > 1) {
                map.fitBounds(bounds);
            }
            window.onresize = function() {
                if (markers.length > 1) {
                    map.fitBounds(bounds);
                } else {
                    map.setCenter(new google.maps.LatLng(theMarker['lat'], theMarker['lng']));
                }
            }
        }
    }

    function getMaxHeight(element) {
        $(element).css('height', 'auto');
        return Math.max.apply(null, $(element).map(function() {
            if (!$(this).is(':visible')) {
                var clone = $(this).clone().css('display', 'block');
                $(this).parent().append(clone);
                var height = clone.outerHeight();
                clone.remove();
                return height;
            } else {
                return $(this).outerHeight();
            }
        }).get());
    }

    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";path=/";
    }

})(jQuery);