<div class="contact-modal">
	<div class="close-modal"></div>
	<div class="custom-heading wpb_content_element ">
		<h2 class="heading-title">Get in touch with an expert!</h2>
		<span class="heading-line primary"></span>
	</div>
	<form class="contact-form" action="<?=esc_url( home_url( '/' ) )?>/wp-content/themes/construction-child/forms.php">
		<div class="formcontent">
			<div class="form-left">
				<input type="text" name="name" placeholder="Name *" required />
				<input type="email" name="email" placeholder="Email Address*" required />
				<input type="text" name="phone" placeholder="Phone *" required />
			</div>
			<div class="form-right">
				<input type="text" name="subject" placeholder="Subject *" required />
				<textarea name="message" placeholder="Message *" required></textarea>
				<button class="btn-highlight">Get a Job Done</button>
			</div>
		</div>
		<p class="themessage"></p>
	</form>
</div>