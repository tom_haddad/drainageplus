<?php
	$main_phone = "01243 282 818";
	if (get_page_template_slug() == "location.php") {
		$main_phone = get_phone_number(get_the_title($post->ID));
	} else if (isset($_COOKIE['location'])) {
		$main_phone = get_phone_number($_COOKIE['location']);
	}
?>
<div id="wpc_599c5a86cec40" class="vc_row wpb_row vc_row-fluid  ">
	<div class="row_inner_wrapper  clearfix" style="padding-top: 0px;padding-bottom: 0px;">
		<div class="row_inner row_fullwidth_content clearfix">
			<div class="dp-cta">
				<div class="container clearfix">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<h2>Call Drainage Plus on <?=$main_phone?></h2>
						<a class="button btn-dark btn-no-shadow" href="https://www.drainageplus.co.uk/contact-us">Book a Job</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="wpc_599c5a86d05a9" class="vc_row wpb_row vc_row-fluid inverted-row ">
	<div class="row_inner_wrapper  clearfix" style="background-color: #000000;padding-top: 50px;padding-bottom: 30px;">
		<div class="row_inner container clearfix">
			<div class="row_full_center_content clearfix">
				<div class="latest-news-row wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
					<div class="vc_column-inner vc_custom_1503410642883">
						<div class="wpb_wrapper">
							<div class="custom-heading wpb_content_element ">
								<h2 class="heading-title" style="color: #ffffff;">Latest News</h2>
								<span class="heading-line " style="background-color: #777777"></span>
							</div>
							<div class="child-page-wrapper recent-news-wrapper ">
							<?php
								$column = 3;
								$carousel_class = 'recent-news-carousel-'.uniqid();
							?>
								<script type="text/javascript">
									jQuery(document).ready(function(){
										"use strict";
										jQuery(".<?=$carousel_class?>").slick({
											rtl: false,
											slidesToShow: 3,
											autoplay: false ,
									                 autoplaySpeed: 3000 ,
									                    speed: 300 ,
											slidesToScroll: 1,
											draggable: false,
											prevArrow: "<span class='carousel-prev'><i class='fa fa-angle-left'></i></span>",
									    				nextArrow: "<span class='carousel-next'><i class='fa fa-angle-right'></i></span>",
									    				responsive: [{
											    breakpoint: 1024,
											    settings: {
											    slidesToShow: 3
											    }
											},
											{
											    breakpoint: 600,
											    settings: {
											    slidesToShow: 2
											    }
											},
											{
											    breakpoint: 480,
											    settings: {
											    slidesToShow: 1
											    }
											}]
										});
									});
								</script>
								<?php
									$count  = 0;
									$args = array(
										'posts_per_page' => 6,
										'post_type'      => 'post',
										'post_status'    => 'publish'
									);
									$recent_posts = new WP_Query( $args );

									if ( $recent_posts->have_posts() ) :
									
													$output = '
													<div class="grid-wrapper grid-'.$column.'-columns grid-row '. $carousel_class .'">';
									
													while ( $recent_posts->have_posts() ) : $recent_posts->the_post(); $count++;
									
														$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
														$time_string = sprintf( $time_string,
															esc_attr( get_the_date( 'c' ) ),
															esc_html( get_the_date() ),
															esc_attr( get_the_modified_date( 'c' ) ),
															esc_html( get_the_modified_date() )
														);
									
														$num_comments = get_comments_number(); // get_comments_number returns only a numeric value
									
														if ( comments_open() ) {
															if ( $num_comments == 0 ) {
																$comments = __('No Comments', 'wpcharming');
															} elseif ( $num_comments > 1 ) {
																$comments = $num_comments . __(' Comments', 'wpcharming');
															} else {
																$comments = __('1 Comment', 'wpcharming');
															}
															$write_comments = '<a href="' . get_comments_link() .'">'. $comments.'</a>';
														} else {
															$write_comments =  __('Comments off.', 'wpcharming');
														}
									
														$output .= '
														<div class="grid-item grid-item grid-sm-6 grid-md-4">';
									
															if( has_post_thumbnail() ) {
															$output .= '
															<div class="grid-thumbnail">
																<a href="'. get_the_permalink() .'" title="'. get_the_title() .'">'. get_the_post_thumbnail( get_the_ID(), 'medium-thumb') .'</a>
															</div>';
															}
									
															$output .= '
															<h4 class="grid-title"><a href="'. get_the_permalink() .'" rel="bookmark">'. get_the_title() .'</a></h4>
									
															<div class="recent-news-meta">
																<span class="post-date"><i class="fa fa-file-text-o"></i> '. $time_string .'</span>
																<span class="comments-link"><i class="fa fa-comments-o"></i> '. $write_comments .'</span>
															</div>
									
															<p>'. get_the_excerpt() .'</p>
									
															<a class="btn btn-light btn-small" href="'. get_the_permalink() .'" title="'. get_the_title() .'">READ MORE</a>
									
														</div>
														';
														if ( $layout == 'grid' ) {
															if ( $count % $column == 0 ) $output .= '
															<div class="clear"></div>';
														}
									
													endwhile;
									
													$output .= '
													</div>';
									
													else:
														$output = __( 'Sorry, there is no child pages under your selected page.', 'wpcharming' );
												endif;
									
												wp_reset_postdata();

												echo $output; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="wpc_599c5a872f123" class="vc_row wpb_row vc_row-fluid  ">
	<div class="row_inner_wrapper  clearfix" style="padding-top: 0px;padding-bottom: 0px;">
		<div class="row_inner row_fullwidth_content clearfix">
			<div class="dp-reviews">
				<div class="container clearfix">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<a class="google">
						</a>
						<a class="yell">
						</a>
						<a class="trustpilot">
						</a>
					</div>
				</div>
			</div>
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner ">
					<div class="wpb_wrapper"></div>
				</div>
			</div>
		</div>
	</div>
</div>