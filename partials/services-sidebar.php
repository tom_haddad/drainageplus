<div class="services-sidebar">
	<h4>Our Services</h4>
	<ul class="service-list">
		<li class="all"><a href="<?=esc_url(home_url('/')).'services'?>">All Drainage Plus services</a></li>
<?php
	$args = array('post_parent' => 17);
	$services = get_children($args);
	foreach($services as $s) {
		$class = str_replace(array('&','/',' ','(',')'), '', strtolower($s->post_title));
?>
		<li class="<?=$class?><?=(get_the_ID() == $s->ID) ? ' cur' : ''?>"><a href="<?=get_permalink($s)?>"><?=$s->post_title?></a></li>
<?php
	}
?>
	</ul>
	<h4>Contact Us</h4>
	<?php include ('contact.php'); ?>
</div>