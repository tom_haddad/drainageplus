<div class="commercial-cta">
	<div class="container clearfix">
		<div class="custom-heading wpb_content_element ">
			<h2 class="heading-title" style="color: #ffffff;">Commercial Services</h2>
			<span class="heading-line " style="background-color: #FFFFFF"></span>
		</div>
		<div class="text">
			<p>
				Commercial drains are susceptible to exactly the same problems as domestic drains, but their size often causes them to encounter the problems – and therefore damage – on a much larger scale. This means that maintenance, regular cleaning and routine testing are vital for keeping commercial drains running as they should. Our highly-trained staff are able to meet all of your commercial plumbing and draining needs.
			</p>
			<a class="button btn-white btn-no-shadow" href="<?=esc_url( home_url( '/' ) )?>commercial">Commercial</a>
		</div>
	</div>
</div>