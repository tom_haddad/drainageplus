<?php
	$data = json_decode(file_get_contents('data/instagram.txt'));
?>
<div class="instagram-section">
	<div class="container clearfix">
		<div class="custom-heading wpb_content_element left">
			<h2 class="heading-title"><i class="fa fa-instagram"></i> Follow us on Instagram #drainageplus</h2>
			<span class="heading-line"></span>
		</div>
		<div class="instagram-grid">
<?php
	$i = 1;
	foreach($data as $in) {
?>
			<div class="instagram-box<?=($i > 4) ? ' hidden' : ''?>">
				<img src="<?=$in->image?>" />
				<div class="in-content">
					<div class="holder">
						<p>
							<?=substr_with_ellipsis($in->content)?>
						</p>
						<div class="buttons">
							<a class="like" href="<?=$in->link?>"></a>
							<a class="comment" href="<?=$in->link?>"></a>
						</div>
					</div>
				</div>
			</div>
<?php
		$i++;
	}
?>
		</div>
<?php
	if (count($data) > 4) {
?>
		<a class="button btn-white-outline load-instagram">Load more</a>
<?php
	}
?>
	</div>
</div>

<?php
function substr_with_ellipsis($string, $chars = 120)
{
    preg_match('/^.{0,' . $chars. '}(?:.*?)\b/iu', $string, $matches);
    $new_string = $matches[0];
    return ($new_string === $string) ? $string : $new_string . '&hellip;';
}