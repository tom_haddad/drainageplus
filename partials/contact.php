<?php
	$main_phone = "01243 282 818";
	if (get_page_template_slug() == "location.php") {
		$main_phone = get_phone_number(get_the_title($post->ID));
	} else if (isset($_COOKIE['location'])) {
		$main_phone = get_phone_number($_COOKIE['location']);
	}
?>
<div class="contact-info-box">
	<p>
		We're available to help you 24 hours a day,	seven days a week.
	</p>
	<div class="contact-info-item">
		<div class="contact-text"><i class="fa fa-phone"></i></div>
		<div class="contact-value"><a href="tel:<?=$main_phone?>" class="phone"><?=$main_phone?></a></div>
	</div>
	<div class="contact-info-item">
		<div class="contact-text"><i class="fa fa-envelope"></i></div>
		<div class="contact-value"><a href="mailto:info@drainageplus.co.uk">info@drainageplus.co.uk</a></div>
	</div>
</div>