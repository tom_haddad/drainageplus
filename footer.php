<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WPCharming
 */

global $wpc_option;

$areas =  get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'location.php'
));

$services = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_parent = '17' AND post_status = 'publish' AND post_type = 'page' ORDER BY menu_order ASC");

?>
<?php
	$main_phone = "01243 282 818";
	if (get_page_template_slug() == "location.php") {
		$main_phone = get_phone_number(get_the_title($post->ID));
	} else if (isset($_COOKIE['location'])) {
		$main_phone = get_phone_number($_COOKIE['location']);
	}
?>

	</div><!-- #content -->

	<div class="clear"></div>

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="footer-connect">
			<div class="container">

				<?php if ( $wpc_option['footer_social'] ) { ?>
				<div class="footer-social">
					<?php if ( $wpc_option['social_text'] ) { ?> <label class="font-heading" for=""><?php echo esc_attr($wpc_option['social_text']); ?></label> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['twitter']) && $wpc_option['footer_use_social']['twitter'] == 1 && $wpc_option['twitter'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['twitter']); ?>" title="Twitter"><i class="fa fa-twitter"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['facebook']) && $wpc_option['footer_use_social']['facebook'] == 1 && $wpc_option['facebook'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['facebook']); ?>" title="Facebook"><i class="fa fa-facebook"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['linkedin']) && $wpc_option['footer_use_social']['linkedin'] == 1 && $wpc_option['linkedin'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['linkedin']); ?>" title="Linkedin"><i class="fa fa-linkedin-square"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['pinterest']) && $wpc_option['footer_use_social']['pinterest'] == 1 && $wpc_option['pinterest'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['pinterest']); ?>" title="Pinterest"><i class="fa fa-pinterest"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['google']) && $wpc_option['footer_use_social']['google'] == 1 && $wpc_option['google'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['google']); ?>" title="Google Plus"><i class="fa fa-google-plus"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['instagram']) && $wpc_option['footer_use_social']['instagram'] == 1 && $wpc_option['instagram'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['instagram']); ?>" title="Instagram"><i class="fa fa-instagram"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['header_use_social']['vk']) && $wpc_option['header_use_social']['vk'] == 1 && $wpc_option['vk'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['vk']); ?>" title="VK"><i class="fa fa-vk"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['header_use_social']['yelp']) && $wpc_option['header_use_social']['yelp'] == 1 && $wpc_option['yelp'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['yelp']); ?>" title="Yelp"><i class="fa fa-yelp"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['header_use_social']['foursquare']) && $wpc_option['header_use_social']['foursquare'] == 1 && $wpc_option['foursquare'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['foursquare']); ?>" title="Foursquare"><i class="fa fa-foursquare"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['flickr']) && $wpc_option['footer_use_social']['flickr'] == 1 && $wpc_option['flickr'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['flickr']); ?>" title="Flickr"><i class="fa fa-flickr"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['youtube']) && $wpc_option['footer_use_social']['youtube'] == 1 && $wpc_option['youtube'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['youtube']); ?>" title="Youtube"><i class="fa fa-youtube-play"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['email']) && $wpc_option['footer_use_social']['email'] == 1 && $wpc_option['email'] !== '' ) { ?><a target="_blank" href="<?php echo wp_kses_post($wpc_option['email']); ?>" title="Email"><i class="fa fa-envelope"></i></a> <?php } ?>
					<?php if ( !empty( $wpc_option['footer_use_social']['rss']) && $wpc_option['footer_use_social']['rss'] == 1 && $wpc_option['rss'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['rss']); ?>" title="RSS"><i class="fa fa-rss"></i></a> <?php } ?>
				</div>
				<?php } ?>
			</div>
		</div>

		<div class="footer-form">
			<div class="container">
				<div class="dp-col-4">
					<a href="<?=esc_url(home_url('/'))?>" class="logo"></a>
				</div>
				<div class="dp-col-8 dp-col-right">	
					<div class="custom-heading wpb_content_element ">
						<h2 class="heading-title">Get in touch with an expert!</h2>
						<span class="heading-line primary"></span>
					</div>
					<?php include('partials/form.php'); ?>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="footer-widgets-area">
				<div class="sidebar-footer footer-columns footer-4-columns clearfix">
					<div id="footer-1" class="footer-1 footer-column widget-area" role="complementary">
						<aside id="text-4" class="widget widget_text">
							<h3 class="widget-title">About Drainage Plus</h3>
							<div class="textwidget">
								<div class="contact-info-box">
									<p>
										Drainage plus is a local family run company who specialise in Drain Unblocking and Drain Repair.
									</p>
									<p>
										With 10+ years experience, you can be sure you have chose the right company
									</p>
									<p>
										<strong>
											We have no Call-Out charge across West Sussex, Surrey & Hampshire and offer Free CCTV Inspections with any clearance.
										</strong>
									</p>
								</div>
							</div>
						</aside>
					</div>
					<div id="footer-2" class="footer-2 footer-column widget-area" role="complementary">
						<aside id="text-5" class="widget widget_text">
							<h3 class="widget-title">DRAINAGE PLUS SERVICES</h3>
							<div class="textwidget">
								<div class="contact-info-box">
									<p class="link-list">
<?php
	foreach($services as $a) {
?>
										<a href="<?=esc_url(home_url('/'))?>services/<?=$a->post_name?>"><?=$a->post_title?></a><br />
<?php
	}
?>
									</p>
									<p class="link-list">
										<a href="<?=esc_url(home_url('/'))?>services">All Services</a><br />
										<a href="<?=esc_url(home_url('/'))?>contact-us">Contact Us</a>
									</p>
								</div>
							</div>
						</aside>
					</div>
					<div id="footer-3" class="footer-3 footer-column widget-area" role="complementary">
						<aside id="text-5" class="widget widget_text">
							<h3 class="widget-title">Areas We Cover</h3>
							<div class="textwidget">
								<div class="contact-info-box">
									<p class="link-list">
<?php
	foreach($areas as $a) {
?>
										<a href="<?=esc_url(home_url('/'))?><?=$a->post_name?>"><?=$a->post_title?></a><br />
<?php
	}
?>
									</p>
								</div>
							</div>
						</aside>
					</div>
					<div id="footer-4" class="footer-4 footer-column widget-area" role="complementary">
						<aside id="text-4" class="widget widget_text">
							<h3 class="widget-title">Contact Us</h3>
							<div class="textwidget">
								<div class="contact-info-box">
									<p>
										We're available to help you 24 hours a day,	seven days a week.
									</p>
									<div class="contact-info-item">
										<div class="contact-text"><i class="fa fa-phone"></i></div>
										<div class="contact-value"><a href="tel:<?=$main_phone?>" class="phone"><?=$main_phone?></a></div>
									</div>
									<div class="contact-info-item">
										<div class="contact-text"><i class="fa fa-envelope"></i></div>
										<div class="contact-value"><a href="mailto:info@drainageplus.co.uk">info@drainageplus.co.uk</a></div>
									</div>
								</div>
								<h3 class="widget-title">Accreditations</h3>
								<div class="accreditations">
									<a href="http://safecontractor.com/home/">
										 <img src="<?=esc_url(home_url('/'))?>wp-content/themes/construction-child/assets/img/footer-accreditation-safecontractor@2x.jpg" />
									</a>
									<a href="https://www.waterjetting.org.uk">
										<img src="<?=esc_url(home_url('/'))?>wp-content/themes/construction-child/assets/img/footer-accreditation-wja@2x.jpg" />
									</a>
								</a>
							</div>
						</aside>
					</div>
				</div>
			</div>
		</div>
		<div class="site-info-wrapper">
			<div class="container">
				<div class="site-info clearfix">
					<div class="copy_text">
						Website built by <a href="https://www.tomhaddad.com" target="_blank">Tom Haddad</a>. Copyright &copy; 2017 Drainage Plus. All rights reserved.
					</div>
					<div class="footer-menu">
						<?php wp_nav_menu( array( 'menu' => 'Footer Right Menu', 'theme_location' => 'footer', 'fallback_cb' => false ) ); ?>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->

</div><!-- #page -->
<script type="text/javascript">
var stylesheet_directory_uri = "<?=get_stylesheet_directory_uri()?>";
</script>
<?php if ( $wpc_option['page_back_totop'] ) { ?>
<div id="btt"><i class="fa fa-angle-double-up"></i></div>
<?php } ?>
</div><!-- .bodyholder -->
<div class="modal-holder">
	<?php include ('partials/contact-modal.php'); ?>
</div>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyArJwnOaKnXXW8bU4LaiPkprxkza8NdiWE"></script>
<?php wp_footer(); ?>
<script type="text/javascript" src="https://dash.reviews.co.uk/widget/float.js" data-store="drainageplus-co-uk" data-color="#12CF6C" data-position="right"></script>
<link href="https://dash.reviews.co.uk/widget/float.css" rel="stylesheet" />
</body>
</html>
