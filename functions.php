<?php
/**
 * Construction child theme.
 */
add_action( 'vc_before_init', 'vc_before_init_actions' );
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

add_action('init', 'checkstuff');

vc_add_shortcode_param( 'service_dropdown', 'service_settings' );

function service_settings( $settings, $value ) {
	$return = '
		<select name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value ' .
             esc_attr( $settings['param_name'] ) . ' ' .
             esc_attr( $settings['type'] ) . '_field">
             <option'.((($value == "Blocked Baths")) ? ' selected' : '').'>Blocked Baths</option>
             <option'.((($value == "Blocked Drains")) ? ' selected' : '').'>Blocked Drains</option>
             <option'.((($value == "Blocked Showers")) ? ' selected' : '').'>Blocked Showers</option>
             <option'.((($value == "Blocked Sinks")) ? ' selected' : '').'>Blocked Sinks</option>
             <option'.((($value == "Blocked Toilets")) ? ' selected' : '').'>Blocked Toilets</option>
             <option'.((($value == "Free CCTV Drain Survey")) ? ' selected' : '').'>Free CCTV Drain Survey</option>
             <option'.((($value == "Drain Cleaning")) ? ' selected' : '').'>Drain Cleaning</option>
             <option'.((($value == "Drain Jetting")) ? ' selected' : '').'>Drain Jetting</option>
             <option'.((($value == "Drain Repair & Relining")) ? ' selected' : '').'>Drain Repair & Relining</option>
             <option'.((($value == "Drain Testing")) ? ' selected' : '').'>Drain Testing</option>
             <option'.((($value == "Blocked/Broken Saniflo Toilet")) ? ' selected' : '').'>Blocked/Broken Saniflo Toilet</option>
        </select>
	';
   return $return;
}

function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'drainage-plus', get_stylesheet_directory_uri() . '/assets/css/stylesheets/screen.css');

    wp_enqueue_script( 'thisisthejs', get_stylesheet_directory_uri() . '/assets/js/thisisthe.js', array( 'jquery' ), '1.0', true);
}

function vc_before_init_actions() {
    require_once('vc-blocks/headerimg.php' ); 
    require_once('vc-blocks/services.php' ); 
    require_once('vc-blocks/cta.php' ); 
    require_once('vc-blocks/testimonials.php' ); 
    require_once('vc-blocks/clients.php' ); 
    require_once('vc-blocks/imgtxtbtn.php' ); 
    require_once('vc-blocks/reviews.php' ); 
    require_once('vc-blocks/map.php' ); 
    require_once('vc-blocks/locations.php' );
    require_once('vc-blocks/largecta.php' ); 
    require_once('vc-blocks/contactform.php' ); 
    require_once('vc-blocks/txtblocksidebar.php' ); 
    require_once('vc-blocks/fullwidthimg.php' ); 
    require_once('inc/template-tags.php');
}

function get_location_info($slug) {
    switch(strtolower($slug)) {
        case 'arundel':
            $stuff = array(
                'lat' => 50.8552,
                'lng' => -0.5551,
            );
            break;
        case 'bognor-regis':
            $stuff = array(
                'lat' => 50.7830,
                'lng' => -0.6731,
            );
            break;
        case 'burgess-hill';
            $stuff = array(
                'lat' => 50.9545,
                'lng' => -0.1287,
            );
            break;
        case 'chichester':
            $stuff = array(
                'lat' => 50.8376,
                'lng' => -0.7749,
            );
            break;
        case 'crawley':
            $stuff = array(
                'lat' => 51.1091,
                'lng' => -0.1872,
            );
            break;
        case 'east-grinstead':
            $stuff = array(
                'lat' => 51.1287,
                'lng' => -0.0145,
            );
            break;
        case 'hampshire':
            $stuff = array(
                'lat' => 51.0577,
                'lng' => -1.3081,
            );
            break;
        case 'haywards-heath':
            $stuff = array(
                'lat' => 50.9990,
                'lng' => -0.1063,
            );
            break;
        case 'horsham':
            $stuff = array(
                'lat' => 51.0629,
                'lng' => -0.3259,
            );
            break;
        case 'lancing':
            $stuff = array(
                'lat' => 50.8282,
                'lng' => -0.3281,
            );
            break;
        case 'littlehampton':
            $stuff = array(
                'lat' => 50.8111,
                'lng' => -0.5387,
            );
            break;
        case 'midhurst':
            $stuff = array(
                'lat' => 50.9869,
                'lng' => -0.7373,
            );
            break;
        case 'petworth':
            $stuff = array(
                'lat' => 50.9867,
                'lng' => -0.6107,
            );
            break;
        case 'selsey':
            $stuff = array(
                'lat' => 50.7310,
                'lng' => -0.7937,
            );
            break;
        case 'shoreham-by-sea':
            $stuff = array(
                'lat' => 50.8342,
                'lng' => -0.2716,
            );
            break;
        case 'southwick-west-sussex':
            $stuff = array(
                'lat' => 50.8552,
                'lng' => -0.5551,
            );
            break;
        case 'steyning':
            $stuff = array(
                'lat' => 50.8353,
                'lng' => -0.2382,
            );
            break;
        case 'surrey':
            $stuff = array(
                'lat' => 51.3148,
                'lng' => -0.56,
            );
            break;
        case 'worthing':
            $stuff = array(
                'lat' => 50.8179,
                'lng' => -0.3729,
            );
            break;
    }
    return $stuff;
}


function get_phone_number($title) {
    switch(strtolower($title)) {
        case 'arundel':
        case 'bognor':
        case 'bognor regis':
        case 'chichester':
        case 'selsey':
            return '01243 282 818';
            break;
        case 'crawley':
            return '01293 366 066';
            break;
        case 'east grinstead':
            return '01342 704 447';
            break;
        case 'haywards heath':
            return '01444 350 020';
            break;
        case 'horsham':
            return '01403 814 601';
            break;
        case 'lancing':
        case 'steyning':
        case 'worthing':
        case 'littlehampton':
            return '01903 800 011';
            break;
        case 'midhurst':
        case 'petworth':
            return '01798 334 420';
            break;
        case 'petersfield':
            return '01730 340 020';
            break;
        case 'hove':
        case 'brighton':
        case 'shoreham by sea':
        case 'shoreham-by-sea':
            return '01273 950 455';
            break;
        case 'salisbury':
            return '01722 534 424';
            break;
        case 'surrey':
        case 'guildford':
            return '01483 654 301';
            break;
        case 'redhill':
            return '01737 570 026';
            break;
        case 'camberley':
            return '01276 590 328';
            break;
        case 'hampshire':
        case 'aldershot':
            return '01252 210 142';
            break;
        case 'basingstoke':
            return '01256 975 991';
            break;
        case 'andover':
            return '01264 874 460';
            break;
        case 'fareham':
            return '01329 394 552';
            break;
        case 'portsmouth':
            return '02393 475 991';
            break;
        case 'southampton':
            return '02380 111 623';
            break;
        case 'lymington':
            return '01590 730 078';
            break;
        case 'winchester':
            return '01962 280 338';
            break;
        default:
            return '01243 282 818';
            break;
    }
}

function replace_content($content)
{
    $main_phone = "01243 282 818";
    if (get_page_template_slug() == "location.php") {
        $main_phone = get_phone_number(get_the_title($post->ID));
    } else if (isset($_COOKIE['location'])) {
        $main_phone = get_phone_number($_COOKIE['location']);
    }
    $content = str_replace('01243 282 818', $main_phone, $content);
    return $content;
}
add_filter('the_content','replace_content');

function checkstuff() {
    $locations = array();
    $r = explode('/', $_SERVER['REQUEST_URI']); 

    $i = 0;
    foreach($r as $a) {
        if (empty($a)) {
            unset($r[$i]);
        }
        $i++;
    }

    $check = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'location.php'
    ));

    foreach($check as $c) {
        $locations[] = $c->post_name;
    }
    if (count($r) >= 2) {
        $loc = $r[count($r) - 1];
        if (in_array($loc, $locations)) {
            setcookie("location", $loc,time()+86400*30,"/");
            $url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
            $url = str_replace($loc, 'services', $url);
            setcookie("location", $loc,time()+86400*30,"/");
            header("Location: ".$url);die();
        }         
    }
}