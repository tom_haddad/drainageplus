<!DOCTYPE html>
<html xmlns="http://http://www.w3.org/1999/xhtml">
<head>
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<title>QUAD STUDENTS</title>
	<style type="text/css">
		body {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			-webkit-font-smoothing: antialiased;
			-webkit-text-size-adjust: none;
			background-color: #ffffff;
		}

		a:hover {
			text-decoration: none !important;
		}

		.ReadMsgBody {
			width: 100%;
		}

		.ExternalClass {
			width: 100%;
		}

		img {
			display: block;
		}

		a:link {
			text-decoration: none;
			color: #797979;
		}

		a:visited {
			text-decoration: none;
			color: #797979;
		}

		a:hover {
			text-decoration: none;
			color: #797979;
		}

		html, body {
			width: 100%;
			height: 100%;
		}

		#outlook a {
			padding: 0;
		}

		.ExternalClass {
			width: 100% !important;
		}

		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font,
		.ExternalClass td, .ExternalClass div {
			line-height: 100%;
		}

		img {
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
		}

		a img {
			border: none;
		}

		.applelinks a {
			color: #222222;
			text-decoration: none;
		}

		.ExternalClass img[class^=Emoji] {
			width: 10px !important;
			height: 10px !important;
			display: inline !important;
		}

		@media screen and (max-device-width: 640px), screen and (max-width: 640px) {
			*[class=TBSstretch] {
				width: 100% !important;
				height: auto !important;
			}

			*[class=TBShide] {
				display: none !important;
			}

			*[class=TBSstack] {
				display: block !important;
				width: 100% !important;
			}

			*[class=emailhidebreaks] br {
				display: none !important;
			}

			*[class=TBSfontsize14] {
				font-size: 14px !important;
			}

			*[class=TBSfontsize16] {
				font-size: 16px !important;
			}

			*[class=TBSfontsize18] {
				font-size: 18px !important;
			}

			*[class=TBSfontsize20] {
				font-size: 20px !important;
			}

			*[class=TBSright] {
				text-align: right;
			}

			*[class=TBScenter] {
				text-align: center;
			}

			*[class=TBSmobi] {
				width: 300px !important;
				height: auto !important;
			}

			*[class=TBSmobimain] {
				width: 320px !important;
				height: auto !important;
			}

			*[class=TBSmobileshow] {
				width: 274px !important;
				height: auto !important;
			}

			*[class=TBSbutton] {
				color: #797979 !important;
				text-decoration: none !important;
				display: block !important;
				background: #000000 !important;
				padding: 5px !important;
				color: #ffffff !important;
				text-align: center !important;
				text-decoration: none !important;
				width: 200px !important;
				margin: 5px !important;
			}

			*[class=TBSupper] {
				display: table-header-group !important;
			}

			*[class=TBSlower] {
				display: table-footer-group !important;
			}
		}

		.pb20 {
			padding-bottom: 20px !important;
		}

		.w320 {
			width: 320px !important;
		}

		.h {
			display: none !important;
		}
	</style>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="yes" name="apple-mobile-web-app-capable" />
	<meta content="yes" name="apple-touch-fullscreen" />
	<meta content="black" name="apple-mobile-web-app-status-bar-style" />
	<meta content="telephone=no" name="format-detection" />
</head>
<body style="min-width:100%;padding:0; margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:100%; background-color:#ffffff;" bgcolor="#ffffff">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="TBSstretch">
		<tbody>
			<tr>
				<td align="center">
					<table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="TBSmobimain" width="619">
						<tbody>
							<tr>
								<td style="padding:10px 0 20px 0;">
									<font style="font-size:20px;font-weight:500;line-height:30px;">
										Thank you for your enquiry.
									</font>
								</td>
							</tr>
							<tr>
								<td style="padding:10px 0 20px 0;">
									<font style="font-size:16px;line-height:26px;">
<?php
	foreach($maildata['data'] as $name => $value) {
?>
										<strong><?=ucwords($name)?></strong>: <?=$value?><br />
<?php
	}
?>
									</font>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">
					<table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="TBSstretch" width="619">
						<tbody>
							<tr>
								<td style="padding:10px 40px 20px 40px;border-top:1px solid #000;">
									<font style="font-size:12px;line-height:26px;">
										Please do not reply to this email as we are not monitoring this inbox. To get in touch with us, please use the details on the <a href="http://www.drainageplus.co.uk/contact-us">Contact Us</a> page of our website.
									</font>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>