<?php
global $wpc_option;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-100153434-1', 'auto');
  ga('send', 'pageview');

</script>
<meta name="google-site-verification" content="VaX6zZjG3lO73uNsbkWM3mqM2LI0UQqNQeF4wxcRR48" />
<meta name="format-detection" content="telephone=no">
</head>

<body <?php body_class(); ?>>
<?php
	$main_phone = "01243 282 818";
	if (get_page_template_slug() == "location.php") {
		$main_phone = get_phone_number(get_the_title($post->ID));
	} else if (isset($_COOKIE['location'])) {
		$main_phone = get_phone_number($_COOKIE['location']);
	}
?>
<div class="bodyholder">
<div id="page" class="hfeed site">

	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'wpcharming' ); ?></a>

	<header id="masthead" class="site-header <?php if ( wpcharming_option('header_fixed') ) echo 'fixed-on' ?>" role="banner">
		<div class="header-wrap">
			<div class="container">
				<div class="site-branding">
					<?php if ( wpcharming_option('site_logo', false, 'url') !== '' ) { ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?=esc_url(home_url('/')).'wp-content/themes/construction-child/assets/img/desktop-logo.svg'?>" alt="<?php get_bloginfo( 'name' ) ?>" />
					</a>
					<?php } else { ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
					<?php } ?>
				</div><!-- /.site-branding -->

				<div class="header-right-wrap clearfix">

					<div class="header-widget">
						<div class="header-right-widgets clearfix">

							<div class="header-extract clearfix">

								<?php if ( $wpc_option['header_social'] ) { ?>
								<div class="extract-element">
									<div class="header-social">
										<?php if ( !empty( $wpc_option['header_use_social']['twitter']) && $wpc_option['header_use_social']['twitter'] == 1 && $wpc_option['twitter'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['twitter']); ?>" title="Twitter"><i class="fa fa-twitter"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['facebook']) && $wpc_option['header_use_social']['facebook'] == 1 && $wpc_option['facebook'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['facebook']); ?>" title="Facebook"><i class="fa fa-facebook"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['linkedin']) && $wpc_option['header_use_social']['linkedin'] == 1 && $wpc_option['linkedin'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['linkedin']); ?>" title="Linkedin"><i class="fa fa-linkedin-square"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['pinterest']) && $wpc_option['header_use_social']['pinterest'] == 1 && $wpc_option['pinterest'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['pinterest']); ?>" title="Pinterest"><i class="fa fa-pinterest"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['google']) && $wpc_option['header_use_social']['google'] == 1 && $wpc_option['google'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['google']); ?>" title="Google Plus"><i class="fa fa-google-plus"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['instagram']) && $wpc_option['header_use_social']['instagram'] == 1 && $wpc_option['instagram'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['instagram']); ?>" title="Instagram"><i class="fa fa-instagram"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['vk']) && $wpc_option['header_use_social']['vk'] == 1 && $wpc_option['vk'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['vk']); ?>" title="VK"><i class="fa fa-vk"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['yelp']) && $wpc_option['header_use_social']['yelp'] == 1 && $wpc_option['yelp'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['yelp']); ?>" title="Yelp"><i class="fa fa-yelp"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['foursquare']) && $wpc_option['header_use_social']['foursquare'] == 1 && $wpc_option['foursquare'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['foursquare']); ?>" title="Foursquare"><i class="fa fa-foursquare"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['flickr']) && $wpc_option['header_use_social']['flickr'] == 1 && $wpc_option['flickr'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['flickr']); ?>" title="Flickr"><i class="fa fa-flickr"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['youtube']) && $wpc_option['header_use_social']['youtube'] == 1 && $wpc_option['youtube'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['youtube']); ?>" title="Youtube"><i class="fa fa-youtube-play"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['email']) && $wpc_option['header_use_social']['email'] == 1 && $wpc_option['email'] !== '' ) { ?><a href="<?php echo wp_kses_post($wpc_option['email']); ?>" title="Email"><i class="fa fa-envelope"></i></a> <?php } ?>
										<?php if ( !empty( $wpc_option['header_use_social']['rss']) && $wpc_option['header_use_social']['rss'] == 1 && $wpc_option['rss'] !== '' ) { ?><a target="_blank" href="<?php echo esc_url($wpc_option['rss']); ?>" title="RSS"><i class="fa fa-rss"></i></a> <?php } ?>
									</div>
								</div>
								<?php } ?>

								<?php if ( $wpc_option['extract_1_text'] || $wpc_option['extract_1_value'] ) { ?>
								<div class="extract-element">
									<span class="header-text"><?php echo esc_attr( $wpc_option['extract_1_text'] ); ?></span> <span class="phone-text primary-color"><span class="phone-text primary-color">CALL US TODAY ON: <a href="tel:<?=$main_phone?>"><?=$main_phone?></a></span>
								</div>
								<?php } ?>

							</div>
						</div>
					</div>
				
					<nav id="site-navigation" class="main-navigation" role="navigation">
						<div id="nav-toggle"><i class="fa fa-bars"></i></div>
						<ul class="wpc-menu">	
<?php
	$pages = get_pages(array('sort_column' => 'menu_order'));
	$ignore = array(1247,1240);
	$toplevel = array(13,17,964,966,968,23,970);
	foreach($toplevel as $p) {
		$p = get_page($p);
		$args = array('post_parent' => $p->ID);
		$has_children = ((count(get_children($args)) > 0 && $p->ID !== 13) || $p->ID == 966) ? true : false;
		if ($p->ID == 966) {
			$the_children =  get_pages(array(
			    'meta_key' => '_wp_page_template',
			    'meta_value' => 'location.php'
			));
		} else {
			$the_children = get_children($args);
		}
		if (!in_array($p->ID, $ignore) && !$p->post_parent) {
?>
							<li class="menu-item menu-item-type-custom menu-item-object-custom<?=($has_children) ? ' menu-item-has-children' : ''?>">
<?php
			if ($has_children) {
?>
								<div class="nav-toggle-subarrow">
									<i class="fa fa-angle-down"></i>
								</div>
<?php
			}
?>
								<a href="<?=get_permalink($p)?>"><?=$p->post_title?></a>
<?php
			if ($has_children) {
?>
								<ul class="sub-menu">
<?php
				foreach($the_children as $c) {	
?>
									<li id="menu-item-881" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item menu-item-881">
										<a href="<?=get_permalink($c)?>"><?=$c->post_title?></a>
									</li>
<?php
				}
?>
								</ul>
<?php
			}
?>
							</li>
<?php
		}
	}
?>
					    </ul>
					</nav><!-- #site-navigation -->
				</div>
			</div>
			
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">